# register-cygwin-filetypes

![](https://img.shields.io/badge/written%20in-batch-blue)

batch script to set up file associations on Windows

- Automatically prefers `mintty` over bash in cmd
- Windows 8: May have to select 'Terminal' from the Open With menu if other applications have expressed interest in the file extension
- No more .bat files ever again

## Changelog

2016-01-24: v2
- Add cygwin64 search paths
- [⬇️ register-cygwin-filetypes-v2.zip](dist-archive/register-cygwin-filetypes-v2.zip) *(989B)*


2014-12-28: v1
- Initial public release
- [⬇️ register-cygwin-filetypes-v1.zip](dist-archive/register-cygwin-filetypes-v1.zip) *(1.25 KiB)*

